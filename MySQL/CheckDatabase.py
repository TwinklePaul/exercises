import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234'
)

testcursor = testdb.cursor()

testcursor.execute("SHOW DATABASES")

for row in testcursor:
    print(row)
