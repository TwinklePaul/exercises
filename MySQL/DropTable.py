import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

""" Drop table after a check """

sql = "DROP TABLE IF EXISTS customers"

testcursor.execute(sql)
