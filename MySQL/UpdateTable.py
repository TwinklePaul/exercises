import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

sql = "UPDATE customers SET address = %s WHERE address = %s"

val = ("Valley 345", "Canyon 123")


testcursor.execute(sql, val)

testdb.commit()

print(testcursor.rowcount, "record(s) affected")
