import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

""" Run after creating user and product table. """

""" 

# sql = "SELECT \
#   users.name AS user, \
#   products.name AS favorite \
#   FROM users \
#   INNER JOIN products ON users.fav = products.id"

# sql = "SELECT \
#   users.name AS user, \
#   products.name AS favorite \
#   FROM users \
#   LEFT JOIN products ON users.fav = products.id"

sql = "SELECT \
  users.name AS user, \
  products.name AS favorite \
  FROM users \
  RIGHT JOIN products ON users.fav = products.id"

testcursor.execute(sql)

myresult = testcursor.fetchall()

for x in myresult:
  print(x) 

"""
