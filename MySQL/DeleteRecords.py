import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()


""" Delete any records satisfying a condn """

sql = "DELETE FROM customers WHERE address = %s"
adr = ("Yellow Garden 2", )


testcursor.execute(sql, adr)

testdb.commit()

print(testcursor.rowcount, "record(s) deleted")
