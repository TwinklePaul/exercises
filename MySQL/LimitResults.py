import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

""" First few """

testcursor.execute("SELECT * FROM customers LIMIT 5")

myresult = testcursor.fetchall()

for x in myresult:
    print(x)


""" Start from another position """

testcursor.execute("SELECT * FROM customers LIMIT 5 OFFSET 2")

myresult = testcursor.fetchall()

for x in myresult:
    print(x)
