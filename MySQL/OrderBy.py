import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

""" Sort in Natural Fashion """

sql = "SELECT * FROM customers ORDER BY name"

testcursor.execute(sql)

myresult = testcursor.fetchall()

for x in myresult:
    print(x)


""" Sort in Reverse """

sql = "SELECT * FROM customers ORDER BY name DESC"

testcursor.execute(sql)

myresult = testcursor.fetchall()

for x in myresult:
    print(x)
