import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

testcursor.execute(
    "SHOW TABLES")

print("Existing Tables: ")
for x in testcursor:
    print(x)


# Insert 1 Row
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
testcursor.execute(sql, val)

testdb.commit()

print(testcursor.rowcount, "record inserted.")


# Insert Multiple Rows
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = [
    ('Peter', 'Lowstreet 4'),
    ('Amy', 'Apple st 652'),
    ('Hannah', 'Mountain 21'),
    ('Michael', 'Valley 345'),
    ('Sandy', 'Ocean blvd 2'),
    ('Betty', 'Green Grass 1'),
    ('Richard', 'Sky st 331'),
    ('Susan', 'One way 98'),
    ('Vicky', 'Yellow Garden 2'),
    ('Ben', 'Park Lane 38'),
    ('William', 'Central st 954'),
    ('Chuck', 'Main Road 989'),
    ('Viola', 'Sideway 1633')
]

testcursor.executemany(sql, val)

testdb.commit()

print(testcursor.rowcount, "record inserted.")


# Display Insertion ID
sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("Michelle", "Blue Village")
testcursor.execute(sql, val)

testdb.commit()

print("1 record inserted, ID:", testcursor.lastrowid)
