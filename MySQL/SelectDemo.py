import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

# View all data

testcursor.execute("SELECT * FROM customers")

result = testcursor.fetchall()

for x in result:
    print(x)


# View some columns

testcursor.execute("SELECT name, address FROM customers")

myresult = testcursor.fetchall()

for x in myresult:
    print(x)


# View 1 row

testcursor.execute("SELECT * FROM customers")

myresult = testcursor.fetchone()

print(myresult)


# Select with where clause and wild cards

sql = "SELECT * FROM customers WHERE address LIKE '%way%'"

testcursor.execute(sql)

myresult = testcursor.fetchall()

for x in myresult:
    print(x)


# Select with where clause

sql = "SELECT * FROM customers WHERE address ='Park Lane 38'"

testcursor.execute(sql)

myresult = testcursor.fetchall()

for x in myresult:
    print(x)


"""
    PREVENTING SQL INJECTION

When query values are provided by the user, you should escape the values.

This is to prevent SQL injections, which is a common web hacking technique to destroy or misuse your database.

The mysql.connector module has methods to escape query values:

"""


sql = "SELECT * FROM customers WHERE address = %s"
adr = ("Yellow Garden 2", )

testcursor.execute(sql, adr)

myresult = testcursor.fetchall()

for x in myresult:
    print(x)
