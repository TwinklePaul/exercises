import mysql.connector

testdb = mysql.connector.connect(
    host='localhost',
    user='twinkle',
    password='1234',
    database='testdatabase'
)

testcursor = testdb.cursor()

# testcursor.execute(
#     "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))")

testcursor.execute(
    "CREATE TABLE customers (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), address VARCHAR(255))")
