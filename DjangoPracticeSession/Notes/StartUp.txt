Startup Steps:

1. Create a virtualenvironment for django.
    Virtualenvs keep django versions separated.
    This ensures that the changes in django version doesn't affect the previously created web dev projects.
    Command:
        virtualenv dj_ds

2. Migrate to the virtualenv directory and activate it.
    Command:
        cd dj_ds
        source bin/activate

3. Once, django gets activated, install django
    Command:
        pip install django

4. Now create a folder to manage reports called as reports_proj using django.
    Command:
        
        
5. The reports_proj file contains a subfolder with same name. Hence, rename it to src

6. Migrate manage.py
    Command: 
        python manage.py migrate

7. Create a superuser for the database
    Command:
        python manage.py createsuperuser
        
        For this project:
        Username: twinkle
        Password: 123

7. Create all the project folders
    Command:
        python manage.py startapp sales
        python manage.py startapp report
        ...

8. Start server
    Command:
        python manage.py runserver
        
        