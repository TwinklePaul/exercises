""" Using Format Strings to display output """

# declaring the descriptions of an individual.
person_name = 'Zed A. Shaw'
person_age = 35
person_height = 74
person_weight = 180
person_eyes = 'Blue'
person_teeth = 'White'
person_hair = 'Brown'

# Displaying the bio of the individual.
print(f"Let's talk about {person_name}.")
print("He's {} inches tall.".format(person_height))
print("He's %d pounds heavy." % person_weight)
print("Actually that's not too heavy.")
print("He's got {eyes} eyes and {hair} hair.".format(
    eyes=person_eyes, hair=person_hair))
print(f"His teeth are usually {person_teeth} depending on the coffee.")

# Tricky Line
print(f"If I add {person_age}, {person_height} and {person_weight}, I get {person_age + person_height + person_weight}.")
