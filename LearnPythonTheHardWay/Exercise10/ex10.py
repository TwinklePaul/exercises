""" Escape Sequences """

tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm a \\ a \\ cat."

fat_cat = """
I'll do a list:
\t* Cat Food
\t* Fishies
\t* Catnip\n\t* Grass
"""

suspense_cat = '''
This is a Study-drill
test-case to view
possible lines
with single-quote.
'''

print(tabby_cat)
print(persian_cat)
print(backslash_cat)
print(fat_cat)
print(suspense_cat)
