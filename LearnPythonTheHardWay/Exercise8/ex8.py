""" Printing... """

# creating a format string
formatter = "%r %r %r %r"

# Passing 4 numeric values as arguments to the format string.
print(formatter % (1, 2, 3, 4))

# Passing 4 strings as arguments to the format string.
print(formatter % ("one", "two", "three", "four"))

# Passing 4 Boolean values as arguments to the format string.
print(formatter % (True, False, False, True))

# Passing the formatters as arguments to the format string.
# Since, the format string is %r,
#        hence, the format string will be displayed as is.
# The %r will give you the “raw programmer’s” version of variable,
#        also known as the “representation.”
print(formatter % (formatter, formatter, formatter, formatter))

# Passing some lines as arguments to the format string.
print(formatter % (
    "I had this thing.",
    "That you could type up right.",
    "but it didn't sing.",
    "So I said goodnight."
))
