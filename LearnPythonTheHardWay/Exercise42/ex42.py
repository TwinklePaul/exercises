""" IS-A, HAS-A, Objects and Classes """

# Animal is-a object (yes, sort of confusing) look at the extra credit


class Animal():
    pass


class Dog(Animal):
    def __init__(self, name):
        self.name = name


class Cat(Animal):
    def __init__(self, name):
        self.name = name


class Person():
    def __init__(self, name):
        self.name = name
        # Person has-a pet of some kind
        self.pet = None


class Employee(Person):
    def __init__(self, name, salary):
        super(Employee, self).__init__(name)
        self.salary = salary


class Fish():
    pass


class Salmon(Fish):
    def __init__(self, name):
        self.name = name


class Halibut(Fish):
    def __init__(self, name):
        self.name = name


# rover is-a Dog
rover = Dog("Rover")

satan = Cat("Satan")


mary = Person("Mary")
mary.pet = satan

frank = Employee("Frank", 120000)
frank.pet = rover


flipper = Fish()
crouse = Salmon("Crouse")
harry = Halibut("Harry")
