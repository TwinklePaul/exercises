try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': 'projectname',
    'description': 'My Project',

    'author': 'Twinkle Paul',
    'author_email': 'twinkle.p@consultadd.com',

    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',

    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['NAME'],
    'scripts': []
}

setup(**config)
