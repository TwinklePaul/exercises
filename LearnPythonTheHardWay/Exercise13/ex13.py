""" Parameters, Unpacking, Variables. """

from sys import argv

# The argv is the “argument variable".
# This variable holds the arguments which
#       you pass to your Python script when you run it.
# Line 12 “unpacks” argv so that,
#       rather than holding all the arguments,
#       it gets assigned to four variables you can work with

script, first, second, third = argv

print(f"The script is called: {script}")
print(f"Your first variable is: {first}")
print(f"Your second variable is: {second}")
print(f"Your third variable is: {third}")
