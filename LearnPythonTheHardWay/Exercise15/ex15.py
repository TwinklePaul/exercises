""" Reading Files. """

from sys import argv

script, filename = argv

txt = open(filename)
# The open() function opens a file, and returns it as a file object.

print(f"Here's your file {filename}: ")
print(txt.read())

print("Type your filename again: ")
file_again = input("> ")

txt_again = open(file_again)
print(txt_again.read())
