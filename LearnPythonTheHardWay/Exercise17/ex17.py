""" More files. """

from sys import argv
from os.path import exists

# exists -> This returns True if a file exists,
#           based on its name in a string as an argument.

script, from_file, to_file = argv

# print(f"Copying from {from_file} to {to_file}.")

# in_file = open(from_file)
# indata = in_file.read()
indata = open(from_file).read()

# print(f"The input file is {len(indata)} bytes long.")

# print(f"Does output file exist? {exists(to_file)}")
# print("Ready, hit RETURN to continue, CTRL+C to abort.")
# input("> ")
print(f"Copying {len(indata)} bytes from {from_file} to {to_file}.")

# out_file = open(to_file, 'w')
# out_file.write(indata)
open(to_file, 'w').write(indata)

print("Alright, All Done!")

# out_file.close()
# in_file.close()
