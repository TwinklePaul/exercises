""" Names, Variables, Code, Functions. """

# A function like scripts with argv


def print_two(*args):
    arg1, arg2 = args
    print(f"arg1: {arg1}, arg2: {arg2}")


# A function with 2 parameters:
def print_two_again(arg1, arg2):
    print(f"arg1: {arg1}, arg2: {arg2}")

# A function with 1 parameter:


def print_one(arg1):
    print(f"arg1: {arg1}")

# A function with no parameter:


def print_none():
    print("I got nothing.")


print_two("Caitlin", "Cisco")
print_two_again("Harrison Wells", "Star Labs")
print_one("Flash!")
print_none()
