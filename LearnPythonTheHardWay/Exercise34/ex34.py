""" Accessing Lists """

animals = ['bear', 'python', 'peacock', 'kangaroo', 'whale', 'platypus']

for index, item in enumerate(animals):
    print(f"At index {index}, animal is {item}.")
