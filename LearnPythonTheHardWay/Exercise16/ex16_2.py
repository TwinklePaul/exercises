""" Read the contents of the file created. """

from sys import argv

script, filename = argv

print(f"I'm going to read {filename}.")
print("Press CTRL+C to end.")
print("Press RETURN to continue reading.")

input("?")

print(f"Opening file {filename}...\n")
target = open(filename, 'r')

print(f"Reading contents of {filename}:")
txt = target.read()
print(txt)

print(f"End of file.\nClosing file {filename}...\n")
target.close()

print(f"{filename} closed. Goodbye.")
