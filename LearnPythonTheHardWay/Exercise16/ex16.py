""" Reading and Writing Files. """

""" 
Some Important Commands: 
    • close — Closes the file. Like File->Save.. in your editor.
    • read — Reads the contents of the file. You can assign the result to a variable.
    • readline — Reads just one line of a text file.
    • truncate — Empties the file. Watch out if you care about the file.
    • write(stuff) — Writes stuff to the file.
"""


from sys import argv
script, filename = argv

print(f"We're going to erase {filename}.")
print(f"If you don't want that, hit CTRL-C (^C).")
print(f"If you want that, hit RETURN.")

input("?")

print(f"Opening the file.")
target = open(filename, 'w')

print(f"Truncating the file. Goodbye!")
target.truncate()

print(f"Now, I'm going to ask you for three lines.")

line1 = input("line 1: ")
line2 = input("line 2: ")
line3 = input("line 3: ")

print(f"I'm going to write these to the file.")

target.write(f"{line1}\n{line2}\n{line3}\n")

print(f"And finally, we close it.")
target.close()
