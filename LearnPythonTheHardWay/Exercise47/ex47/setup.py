try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': 'ex47',
    'description': 'Example - 47: Automated Testing',

    'author': 'Twinkle Paul',
    'author_email': 'twinkle.p@consultadd.com',

    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',

    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ex47'],
    'scripts': []
}

setup(**config)
